import java.util.Date;

import classes.BasicAccount;
import dependencyclasses.Address;
import dependencyclasses.Job;
import dependencyclasses.PreviousJobs;

public class Main {

	public static void main(String[] args) {
		testBasicAccount();
	}

	public static void testBasicAccount() {

		// object creation for Address class
		Address add = new Address("8-2-225/C", "YNagar", "Beside temple", "Hyderabad", "Telangana", "India");
		

		// object creation for Job class
		@SuppressWarnings("deprecation")
		Date dstart = new Date(120, 2, 15);
		Date dend = new Date();
		Job job = new Job("Software Trainnee", dstart, dend);
	

		// object creation for PreviousJobs class
		PreviousJobs pjob = new PreviousJobs("Apple", 2, "Java Developer");
		

		// object creation for BasicAccount class
		@SuppressWarnings({ "rawtypes", "deprecation" })
		BasicAccount ba = new BasicAccount("Nehaa", "Lakshmi", "neha19@.com", "9876543210", "Neha@20",
				new String[] { "Java", "HTML", "sql" }, new String[] { "coding", "Playing games", "Reading books" },
				new Date(120, 8, 20), add, job, pjob);
		System.out.println(ba);

	}

}
