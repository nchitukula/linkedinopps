package interfaces;

import java.util.Date;

import dependencyclasses.Address;
/**
 * Implementing the Interface class IAccount
 * @author nchitukula
 *
 */
public interface IAccount {
	
	/**
	 * Getters for the String firstname
	 */
	public String getFirstName();
	/**
	 * Getters for the String Lastname
	 * @return Lastname.
	 */
	public String getLastName(); 
	/** 
	 * Getters for the EmailAddress
	 * @return emailAddress
	 */
	public String getEmailAddress(); 
	/**
	 * Getters for the Phoneno
	 * @return phoneno.
	 */
	public String getPhoneno() ;
	/**
	 * Getters for the Password
	 * @return Password.
	 */
	public String getPassword();
	/**
	 * Getters for the Skills
	 * @return skills
	 */
	public String[] getSkills(); 
	/**
	 * Getters for the Interests
	 * @return Interests.
	 */
	public String[] getInterests(); 
	/**
	 * Getters for the Dob
	 * @return Dob.
	 */
	public Date getDob();
	/**
	 * Getters for the Address
	 * @return address.
	 */
	public Address[] getAdd();
	

}
