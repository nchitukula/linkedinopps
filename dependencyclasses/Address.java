package dependencyclasses;
/**
 * Implementing the class Address.
 * @author nchitukula
 *
 */
public class Address {
	
	private String houseNo;
	private String streetName;
	private String landmark;
	private String district;
	private String state;
	private String country;
	/**
	 * Constructor for Address
	 * @param houseNo
	 * @param streetName
	 * @param landmark
	 * @param district
	 * @param state
	 * @param country
	 */
	public Address(String houseNo,String streetName,String landmark,String district,String state,String country) {
		this.houseNo = houseNo;
		this.streetName = streetName;
		this.landmark = landmark;
		this.district = district;
		this.state = state;
		this.country = country;
	}
	/**
	 *  Overriding the toString method of Address so that
     * System.out.println is human readable
     */
	 
	@Override
	public String toString() {
		String returnString = "Address Details :\n";
	    returnString +="Houseno        :"+this.houseNo+              "\n";
		returnString +="streetName     :"+this.streetName+            "\n";
		returnString +="landmark       :"+this.landmark+               "\n";
		returnString +="district       :"+this.district+               "\n";
		returnString +="state          :"+this.state+                  "\n";
		returnString +="country        :"+this.country+                 "\n";
		return returnString;
	}

}
