package dependencyclasses;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
/**
 * Implementing the class Job.
 * @author nchitukula
 *
 */
public class Job {
	private String desigination;
	private Date startDate;
	private Date endDate;
	/**
	 * Parameterized constructor.
	 * @param design
	 * @param start
	 * @param end
	 */
	public Job(String design, Date start,Date end) {
		this.desigination = design;
		this.startDate = start;
		this.endDate = end;
	}
	/** Overriding the toString method of Job so that
     * System.out.println is human readable
     */
	@Override
	public String toString() {
		String s =" Job Details \n";
		LocalDate startDateAsLocalDate = this.startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		s +=this.desigination+"\n"; 
		s+="from " +startDateAsLocalDate.getMonth()+","+ startDateAsLocalDate.getYear() +"\n";
		if(this.endDate==null) {
			LocalDate endDateAsLocalDate = this.endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			s+="to"+endDateAsLocalDate.getMonth() +","+ endDateAsLocalDate.getYear();
		} else {
			s+= "currently working";
		} return s;
		}
	}
	


