package dependencyclasses;
/**
 * implementing the class previousJobs.
 * @author nchitukula
 *
 */
public class PreviousJobs {
	
	private String companyName;
	private int     experience;
	private String designation;
	
	/**
	 * Constructor for PreviousJobs
	 * @param pjobName
	 * @param companyName
	 * @param experience
	 * @param designation
	 */

    public PreviousJobs( String companyName, int experience, String designation) {
    	this.companyName = companyName;
    	this.experience = experience;
    	this.designation = designation;
    }
    /**
     * Overriding the toString method of BasicAccount so that
     * System.out.println is human readable
     */
	@Override
	public String toString() {
		String stringto = "The details of an Previous Jobs are ...................\n";
		stringto += "companyName   :" + this.companyName + "\n";
		stringto += "experience    :" + this.experience + "\n";
		stringto += "designation   :" + this.designation + "\n";
		
		return stringto;
	}
    
    
	
	

}
