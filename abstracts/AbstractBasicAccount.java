package abstracts;

import java.util.Date;

import dependencyclasses.Address;
import dependencyclasses.Job;
import dependencyclasses.PreviousJobs;
import interfaces.IAccount;

/**
 * Implementting the abstract class AbstractBAsicAccount.
 * 
 * @author nchitukula
 *
 */
public abstract class AbstractBasicAccount implements IAccount {

	protected String firstName;
	protected String lastName;
	protected String email;
	protected String phoneno;
	protected String password;
	protected String[] skills;
	protected String[] interests;
	protected Date dob;
	protected Address add;
	@SuppressWarnings("unused")
	protected Job job;
	protected PreviousJobs pjob;

	public abstract int getAccountNumber();

	/**
	 * Getter for the set of previous jobs
	 * 
	 * @return
	 */
	public Job[] getPreviousJobs() {
		return getPreviousJobs();
	}
}
