package classes;

import java.util.Date;

import abstracts.AbstractBasicAccount;
import dependencyclasses.Address;
import dependencyclasses.Job;
import dependencyclasses.PreviousJobs;

public class BasicAccount<previousJob> extends AbstractBasicAccount {

	/**
	 * Constructor for BasicAccount.
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param phoneno
	 * @param password
	 * @param skills
	 * @param interests
	 * @param dob
	 * @param add
	 * @param job
	 */
	public BasicAccount(String firstName, String lastName, String email, String phoneno, String password,
			String[] skills, String[] interests, Date dob, Address add, Job job,PreviousJobs pjob) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneno = phoneno;
		this.skills = skills;
		this.interests = interests;
		this.dob = dob;
		this.add = add;
		this.password = password;
		this.job = job;
		this.pjob=pjob;
	}
	/** Overriding the toString method of BasicAccount so that
     * System.out.println is human readable
     */
	@Override
	public String toString() {
		String stringto = "The details of an Basic Account are ...................\n";
		stringto += "firstname  :" + this.firstName + "\n";
		stringto += "lastname   :" + this.lastName + "\n";
		stringto += "email      :" + this.email + "\n";
		stringto += "phoneno    :" + this.phoneno + "\n";
		stringto += "password   :" + this.password + "\n";

		String interests = "[";
		for (int i = 0; i < this.interests.length; i++) {
			interests += "\"" + this.interests[i] + "\" " + " , ";
		}
		interests += "]";
		stringto += "interests are   :" + interests + "\n";

		String skills = "[";
		for (int i = 0; i < this.skills.length; i++) {
			skills += "\" " + this.skills[i] + "\" " + ",";
		}
		skills += "]";
		stringto += "Skills are      : " + skills + "\n";
		stringto += "job             :" + this.job + "\n";
		stringto += "dob       :" + this.dob + "\n";
		stringto += "add       :" + this.add + "\n";
		stringto +="pjob       :"  + this.pjob +"\n";

		return stringto;
	}

	/**
	 * Getters for the String firstname
	 * 
	 * @return firstname.
	 */
	public String getFirstName() {
		return null;
	}

	/**
	 * Getters for the String Lastname
	 * 
	 * @return Lastname.
	 */
	public String getLastName() {
		return null;
	}

	/**
	 * Getters for the EmailAddress
	 * 
	 * @return emailAddress
	 */
	public String getEmailAddress() {
		return null;
	}

	/**
	 * Getters for the Phoneno
	 * 
	 * @return phoneno.
	 */
	public String getPhoneno() {
		return null;
	}

	/**
	 * Getters for the Password
	 * 
	 * @return Password.
	 */
	public String getPassword() {
		return null;
	}

	/**
	 * Getters for the Skills
	 * 
	 * @return skills
	 */
	public String[] getSkills() {
		return null;
	}

	/**
	 * Getters for the Interests
	 * 
	 * @return Interests.
	 */
	public String[] getInterests() {
		return null;
	}

	/**
	 * Getters for the Dob
	 * 
	 * @return Dob.
	 */
	public Date getDob() {
		return null;
	}

	/**
	 * Getters for the Address
	 * 
	 * @return address.
	 */
	public Address[] getAdd() {
		return null;
	}

	@Override
	public int getAccountNumber() {
		// TODO Auto-generated method stub
		return 0;
	}

}
